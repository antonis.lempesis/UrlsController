package eu.openaire.urls_controller.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 *  This controller will test the connectivity with the database and return statistics!
 */
@RestController
@RequestMapping("/impala")
public class ImpalaController {
    private static final Logger logger = LoggerFactory.getLogger(ImpalaController.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @GetMapping("get10PublicationIdsTest")
    public ResponseEntity<?> get10PublicationIdsTest() {

        String query = "SELECT id FROM publication LIMIT 10;";

        try {
            List<String> publications = jdbcTemplate.queryForList(query, String.class);

            return new ResponseEntity<>(publications.toString(), HttpStatus.OK);
        } catch (Exception e) {
            String errorMsg = "Problem when executing \"getAssignmentsQuery\": " + query;
            logger.error(errorMsg, e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorMsg);
        }
    }
}
