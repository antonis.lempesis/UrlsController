package eu.openaire.urls_controller.payloads.requests;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import eu.openaire.urls_controller.models.UrlReport;

import java.util.List;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "workerId",
        "assignmentId",
        "urlReports"
})
public class WorkerReport {

    @JsonProperty("workerId")
    private String workerId;

    @JsonProperty("assignmentRequestCounter")
    private Long assignmentRequestCounter;

    @JsonProperty("urlReports")
    private List<UrlReport> urlReports;

    public WorkerReport(String workerId, Long assignmentRequestCounter, List<UrlReport> urlReports) {
        this.workerId = workerId;
        this.assignmentRequestCounter = assignmentRequestCounter;
        this.urlReports = urlReports;
    }

    public String getWorkerId() {
        return workerId;
    }

    public void setWorkerId(String workerId) {
        this.workerId = workerId;
    }

    public Long getAssignmentRequestCounter() {
        return assignmentRequestCounter;
    }

    public void setAssignmentRequestCounter(Long assignmentRequestCounter) {
        this.assignmentRequestCounter = assignmentRequestCounter;
    }

    public List<UrlReport> getUrlReports() {
        return this.urlReports;
    }

    public void setUrlReports(List<UrlReport> urlReports) {
        this.urlReports = urlReports;
    }

    @Override
    public String toString() {
        return "WorkerReport{" +
                "workerId='" + workerId + '\'' +
                ", assignmentRequestCounter=" + assignmentRequestCounter +
                ", urlReports=" + urlReports +
                '}';
    }
}
