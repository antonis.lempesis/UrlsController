package eu.openaire.urls_controller.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.sql.Timestamp;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "original_url",
        "datasource",
        "workerId",
        "timestamp"
})
public class Assignment {

    @JsonProperty("id")
    private String id;

    @JsonProperty("original_url")
    private String originalUrl;

    @JsonProperty("datasource")
    private Datasource datasource;

    @JsonProperty("workerid")
    private String workerId;

    @JsonProperty("timestamp")
    private Timestamp timestamp;


    public Assignment() {}

    public Assignment(String id, String originalUrl, Datasource datasource, String workerId, Timestamp timestamp) {
        this.id = id;
        this.originalUrl = originalUrl;
        this.datasource = datasource;
        this.workerId = workerId;
        this.timestamp = timestamp;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOriginalUrl() {
        return originalUrl;
    }

    public void setOriginalUrl(String originalUrl) {
        this.originalUrl = originalUrl;
    }

    public Datasource getDatasource() {
        return datasource;
    }

    public void setDatasource(Datasource datasource) {
        this.datasource = datasource;
    }

    public String getWorkerId() {
        return workerId;
    }

    public void setWorkerId(String workerId) {
        this.workerId = workerId;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "Assignment{" +
                "id='" + id + '\'' +
                ", originalUrl='" + originalUrl + '\'' +
                ", datasource=" + datasource +
                ", workerId='" + workerId + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}
