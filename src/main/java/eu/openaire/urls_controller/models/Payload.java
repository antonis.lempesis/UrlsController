package eu.openaire.urls_controller.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.sql.Timestamp;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "original_url",
        "actual_url",
        "timestamp_acquired",
        "mime_type",
        "size",
        "hash",
        "location",
        "provenance"
})
public class Payload {

    @JsonProperty("id")
    private String id;

    @JsonProperty("original_url")
    private String original_url;

    @JsonProperty("actual_url")
    private String actual_url;

    @JsonProperty("timestamp_acquired")
    private Timestamp timestamp_acquired;

    @JsonProperty("mime_type")
    private String mime_type;

    @JsonProperty("size")
    private Long size;  // In bytes.

    @JsonProperty("hash")
    private String hash;

    @JsonProperty("location")
    private String location;

    @JsonProperty("provenance")
    private String provenance;  // "crawl:<PluginName>"

    public Payload() {}

    public Payload(String id, String original_url, String actual_url, Timestamp timestamp_acquired, String mime_type, Long size, String hash, String location, String provenance) {
        this.id = id;
        this.original_url = original_url;
        this.actual_url = actual_url;
        this.timestamp_acquired = timestamp_acquired;
        this.mime_type = mime_type;
        this.size = size;
        this.hash = hash;
        this.location = location;
        this.provenance = provenance;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOriginal_url() {
        return original_url;
    }

    public void setOriginal_url(String original_url) {
        this.original_url = original_url;
    }

    public String getActual_url() {
        return actual_url;
    }

    public void setActual_url(String actual_url) {
        this.actual_url = actual_url;
    }

    public Timestamp getTimestamp_acquired() {
        return timestamp_acquired;
    }

    public void setTimestamp_acquired(Timestamp timestamp_acquired) {
        this.timestamp_acquired = timestamp_acquired;
    }

    public String getMime_type() {
        return mime_type;
    }

    public void setMime_type(String mime_type) {
        this.mime_type = mime_type;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getProvenance() {
        return provenance;
    }

    public void setProvenance(String provenance) {
        this.provenance = provenance;
    }

    @Override
    public String toString() {
        return "Payload{" +
                "id='" + id + '\'' +
                ", original_url='" + original_url + '\'' +
                ", actual_url='" + actual_url + '\'' +
                ", timestamp_acquired='" + timestamp_acquired + '\'' +
                ", mime_type='" + mime_type + '\'' +
                ", size='" + size + '\'' +
                ", md5='" + hash + '\'' +
                ", location='" + location + '\'' +
                ", provenance='" + provenance + '\'' +
                '}';
    }
}
