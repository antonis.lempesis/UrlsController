FROM openjdk:8-jdk-alpine

COPY build/libs/*-SNAPSHOT.jar app.jar

EXPOSE 1880

ENTRYPOINT ["java","-jar","/app.jar", "--spring.config.location=file:///mnt/config/application.properties"]
