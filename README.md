# UrlsController

This is the Controller's Application.<br>
It receives requests coming from the [workers](https://code-repo.d4science.org/lsmyrnaios/UrlsWorker) , constructs an assignments-list with data received from a database and returns the list to the workers.<br>
Then it receives the "WorkerReports" and writes them into the database.<br> 
The database used is the [Impala](https://impala.apache.org/) .<br>
[...] <br>

To install and run the application, run ```git clone```.
Then, provide a file *"S3_minIO_credentials.txt"*, inside the *working directory*.<br>
In the *"S3_minIO_credentials.txt"* file, you should provide the *endpoint*, the *accessKey*, the *secretKey*, the *region* and the *bucket*, in that order, separated by comma.
<br>
Afterwards, execute the ```installAndRun.sh``` script.<br>
If you want to just run the app, then run the script with the argument "1": ```./installAndRun.sh 1```.<br>
<br>
