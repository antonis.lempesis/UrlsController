cd "${0%/*}" || (echo "Could not chdir to this script's dir!" && exit)  # Change the working directory to the script's directory, when running from other location.

justInstall=0

if [[ $# -eq 1 ]]; then
	justInstall=$1
elif [[ $# -gt 1 ]]; then
	echo -e "Wrong number of arguments given: ${#}\nPlease execute it like: script.sh <justInstall: 0 | 1>";	exit 1
fi

gradleVersion="7.3.3"

if [[ justInstall -eq 0 ]]; then

  if [[ ! -d /opt/gradle/gradle-${gradleVersion} ]]; then
    wget https://services.gradle.org/distributions/gradle-${gradleVersion}-bin.zip
    echo -e "\nAsking for sudo, in order to install 'gradle'..\n"
    sudo mkdir /opt/gradle
    sudo apt install -y unzip && sudo unzip -d /opt/gradle gradle-${gradleVersion}-bin.zip
    #ls /opt/gradle/gradle-${gradleVersion}  # For debugging installation
  fi

  export PATH=/opt/gradle/gradle-${gradleVersion}/bin:$PATH

  gradle wrapper --gradle-version=${gradleVersion} --distribution-type=bin

  #gradle tasks  # For debugging installation
  #gradle -v # For debugging installation

  gradle clean
  gradle build
else
  export PATH=/opt/gradle/gradle-${gradleVersion}/bin:$PATH # Make sure the gradle is still accessible (it usually isn't without the "export").
fi

gradle bootRun
